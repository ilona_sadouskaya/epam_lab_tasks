<?php
interface HomoSapiens 
{
}

interface Human extends HomoSapiens 
{
    public function gender();
    
    public function name();
}

abstract class Men implements Human 
{
    public function gender() 
    {
        echo "men";
    }
}

class John extends Men 
{
    public function name() 
    {
        echo "name";
    }
}

$john = new John();

$john->gender();

$hs = $john instanceof HomoSapiens;

$h = $john instanceof HomoSapiens;
?>