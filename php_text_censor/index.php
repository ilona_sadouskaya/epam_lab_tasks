<?php
function sanitaizer($text) 
{
    $regex = '/(?:\d[ \t-]*?){16}/m';
    $matches = [];
    preg_match_all($regex, $text, $matches);

    foreach ($matches as $empty) {
        if (empty($empty)) {
            return $text;
        }
    }

    foreach ($matches as $match) {
        $censor_text = str_pad('', 9, "<removed>");
        $text = str_replace($match, $censor_text, $text);
    }
    return $text;
}

$text = 'My credit card number is 0000 0000 0000 0000';
$text = sanitaizer ($text);

$text = 'Hi! Did you find my number 1234 5313 6323 1453';
$text = sanitaizer ($text);

$text = 'Hi! My phone number + 375 29 111 11 11';
$text = sanitaizer ($text);
?>