<?php
function php_dynamic_calculator (...$result) 
{
    switch ($result[0]) {
        case '+':
            $sum = 0;
            foreach ($result as $n) {
                if ($n = intval ($n)) {
                    $sum += $n;
        }         
    }
            $result = $sum;         
            break;
        case '*':
            $comp = 1;
            foreach ($result as $n) {
                if ($n = intval ($n)) {
                    $comp *= $n;
                }
            }
            $result = $comp;
            break;
        case '-':
            $dif = 0;
            foreach ($result as $n) {
                if ($n = intval ($n)) {
                    $dif -= $n;
                }
            }
            $result = $dif;
            break;
        case '/':
            $quo = 1;
            foreach ($result as $n) {
                if ($n = intval ($n)) {
                    $quo /= $n;
                }
            }
            $result = $quo;
            break;
        default:
            $result = $result [0];
            break;
    }
    return $result;
}
    
$operator = '+';
$result = php_dynamic_calculator ($operator, 1, '23', 'xd');
    
$operator = '*';
$result = php_dynamic_calculator($operator, 2, 'hg', 2, 3);

$operator = '-';
$result = php_dynamic_calculator($operator, '2', '5', '5');

$operator = '/';
$result = php_dynamic_calculator($operator, 1, 4, 'test');

$operator = 3;
$result = php_dynamic_calculator(0, 16, 4, 2);
?>